import { createAppContainer, createBottomTabNavigator } from "react-navigation";

import ContactRoutes from "./routes/contacts.routes";
import ProfileRoutes from "./routes/profile.routes";
import ScannerRoutes from "./routes/scanner.routes";

const AppNavigator = createBottomTabNavigator({
    contact: {
        screen: ContactRoutes,
    },
    scanner: {
        screen: ScannerRoutes,
    },
    profile: {
        screen: ProfileRoutes,
    },
}, {
        initialRouteName: "scanner",
        tabBarPosition: "bottom",
    });

export const AppContainer = createAppContainer(AppNavigator);
