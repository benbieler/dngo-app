import { TestState } from '../state/test.types';

export const TEST_STATE = 'TEST_STATE';

interface TestAction {
    type: typeof TEST_STATE
    payload: TestState
}

export type TestTypes = TestAction;