import { TestTypes, TEST_STATE } from '../types/actions/test.action';

export function testAction(payload: TestTypes) {
  return {
    payload,
    type: TEST_STATE,
  };
}
