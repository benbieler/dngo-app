import { createStore, compose } from 'redux';
import { reduxFirestore } from 'redux-firestore';
import firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';
import 'firebase/firestore';
import reducers from '../reducers';
import { CustomWindow } from '../types/window/devToolsExtention';
import fbConfig from '../config';

declare let window: CustomWindow;

export default function configureStore() {
  const enhancers = [];

  firebase.initializeApp(fbConfig);

    // Dev tools store enhancer
  const devToolsExtension = window.devToolsExtension;
  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
  }

  const createStoreWithMiddleware: any = compose(
        reduxFirestore(firebase),
        ...enhancers,
    )(createStore);

  const store = createStoreWithMiddleware(reducers);

  return store;
}
