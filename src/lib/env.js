import { FIREBASE_API_KEY } from 'react-native-dotenv';

export const env = {
  FIREBASE_API_KEY: FIREBASE_API_KEY,
};
