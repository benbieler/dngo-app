const FIREBASE_API_KEY = 'AIzaSyBYrV_G-V9RENxnvjylZLtP5c6_DLAG5qk';
const FIREBASE_AUTH_DOMAIN = 'todoapp-6ca43.firebaseapp.com';
const FIREBASE_DATABASE_URL = 'https://todoapp-6ca43.firebaseio.com';
const FIREBASE_PROJECT_ID = 'todoapp-6ca43';
const FIREBASE_STORAGE_BUCKET = 'todoapp-6ca43.appspot.com';
const FIREBASE_MESSAGE_SENDER_ID = '268357466962';

// const OAUTH_FB_APP_ID = '541842216177060';
// const OAUTH_GOGL_IOS_CLIENT_ID =
// '268357466962-tj79mb0k02gjtupu0cad8566lhqgrl8d.apps.googleusercontent.com';
// const OAUTH_GOGL_WEB_CLIENT_SECRET = 'nLwonsmtKgvZdPSyLihBcen0';

const fbConfig = Object.freeze({
  apiKey: FIREBASE_API_KEY,
  authDomain: FIREBASE_AUTH_DOMAIN,
  databaseURL: FIREBASE_DATABASE_URL,
  projectId: FIREBASE_PROJECT_ID,
  storageBucket: FIREBASE_STORAGE_BUCKET,
  messagingSenderId: FIREBASE_MESSAGE_SENDER_ID,
});

export default fbConfig;
